# Submitting Issues

* Try [upstream](https://github.com/shadowsocks/shadowsocks-android/releases) version, consider report to them if they have the same issue [upstream CONTRIBUTING.md](https://github.com/shadowsocks/shadowsocks-android/blob/master/CONTRIBUTING.md)
* If you have any questions, check the [FAQ](https://gitlab.com/mahc9kez/shadowsocks-android-foss/blob/master/.github/faq.md)
* Always provide information asked in the template unless you know what you're doing.
* Utilizing Logcat would also be helpful.
