* Add support for plain method
* Hardware acceleration for AES
* TCP fast open and hosts are deprecated for now
* Local DNS also running over TCP
* Support for Android 11
* Add machine learning technology powered built-in QR code scanner
